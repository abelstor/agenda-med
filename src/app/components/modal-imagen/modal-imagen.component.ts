import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { ModalImagenService } from '../../services/modal-imagen.service';
import { FileUploadService } from '../../services/file-upload.service';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.component.html',
  styles: [
  ]
})
export class ModalImagenComponent implements OnInit {

  public imagenSubir: File | any;
  public imgTemp: any = null;

  constructor( public modalImagenService: ModalImagenService,
               private fileUploadService: FileUploadService ) { }

  ngOnInit(): void {
  }

  cerrarModal() {
    this.imgTemp = null;
    this.modalImagenService.cerrarModal();
  }

  cambiarImagen(event: any): any {
    this.imagenSubir = event.target.files[0];
    if(!this.imagenSubir){
      return this.imgTemp = null;
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.imagenSubir);
    
    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }
    
  }

  subirImagen() {

    const id = this.modalImagenService.id;
    const tipo = this.modalImagenService.tipo;

    this.fileUploadService
        .actualizarFoto( this.imagenSubir, tipo, id )
        .then( img => { 
          Swal.fire({
            title: 'OK!',
            text: `Imagen Actualizada`,
            icon: 'success'
          });
          this.modalImagenService.nuevaImagen.emit( img );
          this.cerrarModal();
        }).catch( err => {
          Swal.fire({
            title: 'Carga no exitosa!',
            text: `${ err.error.msg }`,
            icon: 'error'
          });
        })
        
  }

}

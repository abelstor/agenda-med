import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../guards/auth.guard';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { PerfilComponent } from './perfil/perfil.component';
import { UsuariosComponent } from './mantenimientos/usuarios/usuarios.component';


const routes: Routes = [
    { 
        path: 'dashboard', 
        component: PagesComponent,
        canActivate: [ AuthGuard ],
        children: [
          { path: '', component: DashboardComponent, data: { titulo: 'Principal'}},
          { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Ajustes'}},
          { path: 'grafica1', component: Grafica1Component, data: { titulo: 'Gráficas'}},
          { path: 'progress', component: ProgressComponent, data: { titulo: 'Barra de progreso'}},
          { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas'}},
          { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil'}},
          { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RxJs'}},
          

          //Mantenimientos
          { path: 'usuarios', component: UsuariosComponent, data: { titulo: 'Usuario de aplicación'}},

        ],


    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {}

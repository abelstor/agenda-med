import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

import { UsuarioService } from '../../services/usuario.service';
import { FileUploadService } from '../../services/file-upload.service';

import { Usuario } from '../../models/usuario.model';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {

  public perfilForm: FormGroup | any;
  public usuario: Usuario | any;
  public imagenSubir: File | any;
  public imgTemp: any = null;

  constructor( private fb: FormBuilder,
               private usuarioService: UsuarioService,
               private fileUploadService: FileUploadService ) {
      this.usuario = usuarioService.usuario;   
  }

  ngOnInit(): void {

    this.perfilForm = this.fb.group({
      nombre: [this.usuario.nombre, Validators.required ],
      email:  [this.usuario.email, [Validators.required, Validators.email] ],
    });
  }

  actualizarPerfil() {
    this.usuarioService.actualizarPerfil(this.perfilForm.value)
        .subscribe( () => {
          const { nombre, email } = this.perfilForm.value;
          this.usuario.nombre = nombre;
          this.usuario.email = email;
          Swal.fire({
            title: 'Usuario Actualizado!',
            text: `${ nombre } ... ${ email }`,
            icon: 'success' //success, info,
            // confirmButtonText: 'cool'
          });
          this.perfilForm.reset(); //TODO limpiar el formulario
        }, err => Swal.fire({
              title: 'Error!',
              text: `${ err.error.msg }`,
              icon: 'error'
        }));
  }

  cambiarImagen(event: any): any {
    this.imagenSubir = event.target.files[0];
    if(!this.imagenSubir){
      return this.imgTemp = null;
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.imagenSubir);
    
    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }
    
  }

  subirImagen() {
    this.fileUploadService
        .actualizarFoto( this.imagenSubir, 'usuarios', this.usuario.uid )
        .then( img => { 
          this.usuario.img = img;
          Swal.fire({
            title: 'OK!',
            text: `Imagen Actualizada`,
            icon: 'success'
          });
        }).catch( err => {
          Swal.fire({
            title: 'Carga no exitosa!',
            text: `${ err.error.msg }`,
            icon: 'error'
          });
        })
        
  }

}

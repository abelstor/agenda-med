import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';
import { delay } from 'rxjs/operators';

import { Usuario } from '../../../models/usuario.model';
import { UsuarioService } from '../../../services/usuario.service';
import { BusquedasService } from '../../../services/busquedas.service';
import { ModalImagenService } from '../../../services/modal-imagen.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: [
  ]
})
export class UsuariosComponent implements OnInit, OnDestroy {

  public totalUsuarios: number = 0;
  public usuarios: Usuario[] = [];
  public usuariosTemp: Usuario[] = [];

  public imgSubs: Subscription | any;
  public desde: number = 0;
  public cargando: boolean = true;
  public id: string | any;
  public img: string | any;

  constructor( private usuarioService: UsuarioService,
               private busquedasService: BusquedasService,
               private modalImagenService: ModalImagenService ) { }
  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.cargarUsuarios();
    this.imgSubs = this.modalImagenService.nuevaImagen
        .pipe(
          delay(300)
        )
        .subscribe( img => {
          this.cargarUsuarios()
        });
  }

  cargarUsuarios() {
    this.cargando = true;
    this.usuarioService.cargarUsuarios(this.desde)
    .subscribe( ({ total, usuarios }) => {
      this.totalUsuarios = total;
      this.usuarios = usuarios;
      this.usuariosTemp = usuarios;
      this.cargando = false;
    })
  }

  cambiarPagina( valor: number ) {
    this.desde += valor;

    if ( this.desde < 0 ) {
      this.desde = 0;
    } else if ( this.desde >= this.totalUsuarios ) {
      this.desde -= valor;
    }

    this.cargarUsuarios();
  }

  buscar( termino: string ) {
    if( termino.length === 0) {
      return this.usuarios = this.usuariosTemp;
    } else {
      return this.busquedasService.buscar( 'usuarios', termino )
          .subscribe( resultados => {
            this.usuarios = resultados;
          });
    }
  }

  eliminarUsuario( usuario: Usuario ) {

    if( usuario.uid === this.usuarioService.uid ) {
      return Swal.fire({
        title: 'Error',
        text: `No puede eliminarse a sí mismo: ${ usuario.nombre }`,
        icon: 'warning', // warning
      });
    } else {

      return Swal.fire({
        title: '¿Está seguro?',
        text: `Está a punto de eliminar el usuario ${ usuario.nombre }`,
        icon: 'question', // warning
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy de acuerdo!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.usuarioService.eliminarUsuario( usuario )
              .subscribe( resp => {
                this.cargarUsuarios();
                Swal.fire(
                  `Usuario eliminado!`,
                  `El usuario ${ usuario.nombre } se eliminó correctamente!`,
                  'success'
                );
              });
        }
      })
    }
  }

  cambiarRole( usuario: Usuario) {
    this.usuarioService.guardarUsuario( usuario )
        .subscribe( resp => {
          console.log(resp);
        })
  }

  abrirModal( usuario: Usuario ) {
    this.id = usuario.uid;
    this.img = usuario.img
    this.modalImagenService.abrirModal('usuarios', this.id, this.img );
  }

}

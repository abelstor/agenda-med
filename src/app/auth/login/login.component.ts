import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';

import Swal from 'sweetalert2'

import { UsuarioService } from '../../services/usuario.service';

declare const gapi:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formSubmitted = false;

  public auth2: any;

  public loginForm = this.fb.group({

    email: [localStorage.getItem('email') || '', [ Validators.required, Validators.email ]],
    password: ['', [ Validators.required ]],
    remember: [false, [ Validators.requiredTrue ]],
  });

  constructor( private fb: FormBuilder,
               private usuarioService: UsuarioService,
               private router: Router,
               private ngZone: NgZone ) { }

  ngOnInit(): void {
    this.renderButton();
  }

  login() {
    this.usuarioService.login( this.loginForm.value )
        .subscribe( resp => {

          if( this.loginForm.get( 'remember')?.value ) {
            localStorage.setItem( 'email', this.loginForm.get('email')?.value );
          } else {
            localStorage.removeItem( 'email' );
          }
          // Navegar al dashboard
          this.router.navigateByUrl('/dashboard');
        }, (err) => {
          Swal.fire({
            allowOutsideClick: false,
            title: 'Error!',
            text: `${err.error.msg}`,
            icon: 'error', //success, info,
            // confirmButtonText: 'cool'
          });
        });
            
  }

  renderButton() {
    gapi.signin2.render('sign', {
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'dark',
    });
    this.startApp();
  }

  async startApp() {

    await this.usuarioService.googleInit();
    this.auth2 = this.usuarioService.auth2;
    this.attachSignin(document.getElementById('sign'));
  }

  attachSignin(element: any) {
    this.auth2.attachClickHandler(element, {},
        (googleUser: any) => {
          const id_token = googleUser.getAuthResponse().id_token;
          this.usuarioService.loginGoogle( id_token )
              .subscribe( resp => {
                // Navegar al dashboard
                this.ngZone.run(() => {
                  this.router.navigateByUrl('/dashboard');
                })
              });
        }, (error: any) => {
          alert(JSON.stringify(error, undefined, 2));
        });
  }


}
